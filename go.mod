module gitlab.com/golang_development/go_di_example

go 1.22.1

require (
	github.com/mattn/go-sqlite3 v1.14.22
	go.uber.org/dig v1.17.1
)
