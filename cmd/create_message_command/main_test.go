package main

import (
	"testing"

	"gitlab.com/golang_development/go_di_example/pkg/di"
)

func TestRunCommand(t *testing.T) {
	if err := runCommand(di.EnvTest); err != nil {
		t.Fatal(err)
	}
}
