package main

import (
	"errors"
	"log"
	"os"

	"gitlab.com/golang_development/go_di_example/internal/model"
	"gitlab.com/golang_development/go_di_example/internal/repository"
	"gitlab.com/golang_development/go_di_example/pkg/di"
)

func main() {
	if err := runCommand(di.EnvProd); err != nil {
		log.Fatal(err)
	}
}

func runCommand(env di.Environment) error {
	c := di.BuildContainer(env)

	return c.Invoke(func(messageRepository *repository.MessageRepository) error {
		if len(os.Args) < 2 {
			return errors.New("pattern arg is required")
		}

		pattern := os.Args[1]
		message := model.NewMessage(pattern)

		return messageRepository.Create(message)
	})
}
